// tag::sample[]
package hello;

import javax.persistence.*;

@Entity
public class PhoneNumber {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    private String phonenumber;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer")
    private Customer customer;

    private boolean active;

    protected PhoneNumber() {}

    public PhoneNumber(String number, Customer customer) {
        this.phonenumber = number;
        this.customer = customer;
        this.active = true;
    }

    @Override
    public String toString() {
        return String.format(
                "\u001B[34;1mPhone_Number[id=%d, customer_id=%d, phone_number=%s, is_active=%b]\u001B[0m",
                id, customer.getId(), phonenumber, active);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}

