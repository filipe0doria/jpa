package hello;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Calendar;
import java.util.Scanner;

@SpringBootApplication
public class Application {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }

    @Bean
    public CommandLineRunner demo(CustomerRepository customerRepository, OrdersRepository ordersRepository, PhoneNumbersRepository numbersRepository) {
        return (args) -> {
            // save a couple of customers
            Customer customer1 = customerRepository.save(new Customer("Jack", "Bauer"));
            Customer customer2 = customerRepository.save(new Customer("Chloe", "O'Brian"));
            Customer customer3 = customerRepository.save(new Customer("Kim", "Bauer"));
            Customer customer4 = customerRepository.save(new Customer("David", "Palmer"));
            Customer customer5 = customerRepository.save(new Customer("Michelle", "Dessler"));
            Customer customer6 = customerRepository.save(new Customer("Michelle", "Obama"));
            Customer customer7 = customerRepository.save(new Customer("Philip", "Doria"));
            Customer customer8 = customerRepository.save(new Customer("Sonic", "TheEdgehog"));

            System.out.println("Query user first name:");
            Scanner read = new Scanner(System.in);
            String name = read.nextLine();

            log.info("Query user last name:");
            String surname = read.nextLine();

            Customer customer9 = customerRepository.save(new Customer(name,surname));

            // fetch all customers
            log.info("\u001B[31;1mCustomers found with findAll():\u001B[0m");
            log.info("\u001B[31;1m-------------------------------");
            for (Customer customer : customerRepository.findAll()) {
                log.info(customer.toString());
            }
            log.info("");


            ordersRepository.save(new Order(Calendar.getInstance(), customer1));
            ordersRepository.save(new Order(Calendar.getInstance(), customer2));
            ordersRepository.save(new Order(Calendar.getInstance(), customer3));
            ordersRepository.save(new Order(Calendar.getInstance(), customer4));
            ordersRepository.save(new Order(Calendar.getInstance(), customer5));
            ordersRepository.save(new Order(Calendar.getInstance(), customer6));
            ordersRepository.save(new Order(Calendar.getInstance(), customer7));
            ordersRepository.save(new Order(Calendar.getInstance(), customer8));

            // fetch all orders
            log.info("\u001B[31;1mOrders found with findAll():\u001B[0m");
            log.info("\u001B[31;1m-------------------------------\u001B[0m");
            for (Order order : ordersRepository.findAll()) {
                log.info(order.toString());
            }
            log.info("");

            numbersRepository.save(new PhoneNumber("0234783274", customer6));
            numbersRepository.save(new PhoneNumber("1234567890", customer6));
            numbersRepository.save(new PhoneNumber("0234712274", customer6));
            numbersRepository.save(new PhoneNumber("1234567891", customer6));
            numbersRepository.save(new PhoneNumber("0234783274", customer8));
            numbersRepository.save(new PhoneNumber("1234567890", customer8));

            // fetch all phoneNumber
            log.info("\u001B[31;1mPhoneNumbers found with findAll():\u001B[0m");
            log.info("\u001B[31;1m-------------------------------\u001B[0m");
            for (PhoneNumber number : numbersRepository.findAll()) {
                log.info(number.toString());
            }
            log.info("");

            // fetch an individual phone number by ID
            numbersRepository.findById(21L)
                    .ifPresent(number -> {
                        log.info("\u001B[31;1mPhone number found with findById(21L):\u001B[0m");
                        log.info("\u001B[31;1m--------------------------------\u001B[0m");
                        log.info(number.toString());
                        log.info("");
                    });

            // update an individual phone number by ID
            numbersRepository.findById(21L)
                    .ifPresent(number -> {
                        log.info("\u001B[31;1mPhone number updated by setActive(false) after being found with findById(21L):\u001B[0m");
                        log.info("\u001B[31;1m--------------------------------\u001B[0m");
                        number.setActive(false);
                        log.info(number.toString());
                        log.info("");
                    });

            numbersRepository.deleteById(22L);

            // fetch all phoneNumber updated after delete
            log.info("\u001B[31;1mPhoneNumbers found with findAll()  updated after delete of id 22:\u001B[0m");
            log.info("\u001B[31;1m-------------------------------\u001B[0m");
            for (PhoneNumber number : numbersRepository.findAll()) {
                log.info(number.toString());
            }
            log.info("");


            // fetch an individual customer by ID
            customerRepository.findById(1L)
                    .ifPresent(customer -> {
                        log.info("Customer found with findById(1L):");
                        log.info("--------------------------------");
                        log.info(customer.toString());
                        log.info("");
                    });

            // fetch customers by first name
            log.info("Customer found with findByFirstName('Sonic'):");
            log.info("--------------------------------------------");
            customerRepository.findByFirstName("Sonic").forEach(sonic -> {
                log.info(sonic.toString());
            });

            // fetch customers by last name
            log.info("Customer found with findByLastName('Doria'):");
            log.info("--------------------------------------------");
            customerRepository.findByLastName("Doria").forEach(doria -> {
                log.info(doria.toString());
            });
            // for (Customer bauer : customerRepository.findByLastName("Bauer")) {
            // 	log.info(bauer.toString());
            // }
            log.info("");

        };

    }
}