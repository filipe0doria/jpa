package hello;

import javax.persistence.*;
import java.util.Calendar;

@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Temporal(TemporalType.TIMESTAMP)
    private Calendar purchasedAt;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer")
    private Customer customer;

    protected Order(){}

    public Order(Calendar calendar, Customer newCustomer) {
        this.purchasedAt = calendar;
        this.customer = newCustomer;
    }

    @Override
    public String toString() {
        return String.format(
                "\u001B[34;1mOrder[id=%d, purchasedAt=%s, customer=%d]\u001B[0m",
                id, purchasedAt.getTime().toString(), customer.getId());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Calendar getPurchasedAt() {
        return purchasedAt;
    }

    public void setPurchasedAt(Calendar purchasedAt) {
        this.purchasedAt = purchasedAt;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
