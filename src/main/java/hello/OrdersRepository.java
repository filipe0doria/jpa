package hello;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrdersRepository extends CrudRepository<Order, Long> {

    List<Order> findByCustomerId(Long customerID);
}
