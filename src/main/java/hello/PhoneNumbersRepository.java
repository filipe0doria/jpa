package hello;

import org.springframework.data.repository.CrudRepository;

public interface PhoneNumbersRepository extends CrudRepository<PhoneNumber, Long> {

}
